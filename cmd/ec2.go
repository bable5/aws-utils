package cmd

import (
	"fmt"
	"log"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/spf13/cobra"
	"gitlab.com/bable5/aws-utils/awsutils"
)

func init() {
	rootCmd.AddCommand(ec2Cmd)

	ec2Cmd.Flags().StringP("tag", "t", "", "Tag on the ec2 instances(<Name>:<Value> format)")
}

var ec2Cmd = &cobra.Command{
	Use:   "ec2",
	Short: "AWS EC2 Utilities.",
	Long:  `Utilities to simplify interacting with the EC2 API`,
	Run: func(cmd *cobra.Command, args []string) {
		session := session.New()
		filters := []*string{}

		tags, _ := cmd.Flags().GetString("tag")

		filters = append(filters, &tags)

		ips, err := awsutils.ListInstanceIpsByTag(session, filters)

		if err != nil {
			log.Fatal(err)
		}

		for _, ip := range ips {
			fmt.Println(ip)
		}
	},
}
