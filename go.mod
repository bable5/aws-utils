module gitlab.com/bable5/aws-utils

go 1.12

require (
	github.com/aws/aws-sdk-go v1.28.5
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.6.2
)
