package awsutils

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
)

func ListInstanceIpsByTag(session *session.Session, tags []*string) ([]*string, error) {

	svc := ec2.New(session)

	ec2Search := &ec2.DescribeInstancesInput{
		Filters: []*ec2.Filter{
			&ec2.Filter{
				Name:   aws.String("tag"),
				Values: tags,
			},
		},
	}

	res, err := svc.DescribeInstances(ec2Search)

	if err != nil {
		return nil, err
	}

	ips := []*string{}

	// TODO: Paging. While next token is not nil
	for _, r := range res.Reservations {
		for _, i := range r.Instances {
			ips = append(ips, i.PrivateIpAddress)
		}
	}

	return ips, nil
}
